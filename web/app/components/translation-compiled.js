'use strict';

(function () {
  'use strict';

  angular.module('myApp.translation', []).value("property", {
    name: "Nutrition in 100g of",
    kcal: "Kcal",
    protein_g: "Protein(g)",
    fat_g: "Fat(g)",
    carb_g: "Carbohydrate(g)",
    fiber_g: "Fiber(g)",
    sugar_g: "Sugar(g)",
    calcium_mg: "Calcium(mg)",
    iron_mg: "Iron(mg)",
    sodium_mg: "Sodium(mg)",
    cholesterol_mg: "Cholesterol(mg)"
  });
})();

//# sourceMappingURL=translation-compiled.js.map