(function () {
  'use strict';
    
  angular.module('myApp.filters', [])
    .filter('i18n', ['property', function (cl) {
      return function (key, p) {
        return (cl[key] && p) ? cl[key].replace('@{}@', p) : cl[key];
      }
    }]);
}());