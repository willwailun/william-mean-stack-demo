'use strict';

(function () {
	'use strict';

	angular.module('myApp', ['ngRoute', 'myApp.translation', 'myApp.filters', 'myApp.home', 'myApp.foods']).config(['$routeProvider', function ($routeProvider) {
		$routeProvider.otherwise({ redirectTo: '/' });
	}]).run(['$rootScope', '$route', function ($rootScope, $route) {
		$rootScope.$on('$routeChangeSuccess', function () {
			document.title = $route.current.title;
		});
	}]);
})();

//# sourceMappingURL=app-compiled.js.map