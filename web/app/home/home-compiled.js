'use strict';

(function () {
	'use strict';

	angular.module('myApp.home', ['ngRoute']).config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/', {
			title: "Home | Know Your Food",
			templateUrl: 'home/home.html',
			controller: 'HomeCtrl'
		});
	}]).controller('HomeCtrl', function ($scope, $http) {});
})();

//# sourceMappingURL=home-compiled.js.map