(function () {
  "use strict";

  angular.module("myApp.foods", ["ngRoute"])

    .config(["$routeProvider", function ($routeProvider) {
      $routeProvider.when("/foods", {
        title: "Foods | Know Your Food",
        templateUrl: "foods/foods.html",
        controller: "FoodsCtrl"
      });
    }])

    .controller("FoodsCtrl", function ($scope, $http) {
      const limit = 10;
      const serverUrl = "http://52.193.180.123:81";
      $scope.foods = [];
      $scope.headers = [];
      $scope.currentPage = 1;
      $scope.headers = [];
      $scope.orderby = null;
      $scope.asc = false;
      $scope.searchText = "";
      $scope.selectedField = "";

      /**
       * Scope Helpers
       */
      $scope.nextPage = function () {
        $scope.currentPage++;
      };

      $scope.previousPage = function () {
        1 < $scope.currentPage && $scope.currentPage--;
      };

      $scope.search = function () {
        $scope.currentPage = 1;
        $scope.orderby = null;
        $scope.asc = false;
        getFoods();
      };

      $scope.update = function (foodId, key, value) {
        updateFood(foodId, key, value);
      };

      /**
       * Private Helpers
       */
      let getFoods = function () {
        $http.get(`${serverUrl}/foods`, {
            params: {
              skip: ($scope.currentPage - 1) * limit,
              limit: limit,
              orderby: $scope.orderby,
              asc: $scope.asc,
              search: $scope.searchText
            }
          })
          .then(function (res) {
            let d = res.data;
            $scope.headers = d && d.length && _.without(_.keys(d[0]), "_id", "uuid", "created_at", "updated_at", "score") || [];
            $scope.foods = d && _.map(d, (x) => {
                return _.pick(x, ...$scope.headers, "_id")
              }) || [];
          }, function (res) {
            console.log(res)
          });
      };

      let updateFood = function (foodId, key, value) {
        $http.post(`${serverUrl}/foods/${foodId}`, _.object([key], [value]))
          .then(function (res) {
            getFoods();
          }, function (res) {
            console.log(res)
          })
      }


      /**
       *  Watchers
       */
      $scope.$watch('currentPage', function () {
        getFoods();
      });
      $scope.$watchGroup(['orderby', 'asc'], function () {
        $scope.currentPage = 1;
        getFoods();
      });


      /**
       * Init
       */
      getFoods();
    })
    .directive("editableTable", function () {
      return {
        restrict: "E",
        scope: {items: "=", headers: "=", orderby: "=", asc: "=", update: "="},
        template: `
        <table class="ui sortable celled compact collapsing small table very basic">
          <thead>
          <tr>
              <th ng-repeat="header in headers" ng-click="($parent.orderby === header) && ($parent.asc = !$parent.asc) || ($parent.orderby = header)">{{header | i18n}}</th>
          </tr>
          </thead>
          <tbody>
          <tr ng-repeat="item in items">
              <td ng-repeat="(key, value) in item" style="text-transform: capitalize;" ng-if="key !== '_id'" ng-init="selected = false" ng-click="selected = true">
              <div ng-if="!selected">
                {{value}}
              </div>
              <div ng-if="selected">
                <form ng-submit="update(item._id, key, updateValue)">
                  <div class="ui input" ng-init="updateValue = value">
                    <input type="text" autofocus ng-model="updateValue" style="padding:0;width:100%;"/>
                  </div>
                </form>
              </div>
              </td>
          </tr>
          </tbody>
      </table>
        `
      };
    })
}());