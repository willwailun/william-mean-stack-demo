"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

(function () {
  "use strict";

  angular.module("myApp.foods", ["ngRoute"]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider.when("/foods", {
      title: "Foods | Know Your Food",
      templateUrl: "foods/foods.html",
      controller: "FoodsCtrl"
    });
  }]).controller("FoodsCtrl", function ($scope, $http) {
    var limit = 10;
    var serverUrl = "http://52.193.180.123:81";
    $scope.foods = [];
    $scope.headers = [];
    $scope.currentPage = 1;
    $scope.headers = [];
    $scope.orderby = null;
    $scope.asc = false;
    $scope.searchText = "";
    $scope.selectedField = "";

    /**
     * Scope Helpers
     */
    $scope.nextPage = function () {
      $scope.currentPage++;
    };

    $scope.previousPage = function () {
      1 < $scope.currentPage && $scope.currentPage--;
    };

    $scope.search = function () {
      $scope.currentPage = 1;
      $scope.orderby = null;
      $scope.asc = false;
      getFoods();
    };

    $scope.update = function (foodId, key, value) {
      updateFood(foodId, key, value);
    };

    /**
     * Private Helpers
     */
    var getFoods = function getFoods() {
      $http.get(serverUrl + "/foods", {
        params: {
          skip: ($scope.currentPage - 1) * limit,
          limit: limit,
          orderby: $scope.orderby,
          asc: $scope.asc,
          search: $scope.searchText
        }
      }).then(function (res) {
        var d = res.data;
        $scope.headers = d && d.length && _.without(_.keys(d[0]), "_id", "uuid", "created_at", "updated_at", "score") || [];
        $scope.foods = d && _.map(d, function (x) {
          var _ref;

          return (_ref = _).pick.apply(_ref, [x].concat(_toConsumableArray($scope.headers), ["_id"]));
        }) || [];
      }, function (res) {
        console.log(res);
      });
    };

    var updateFood = function updateFood(foodId, key, value) {
      $http.post(serverUrl + "/foods/" + foodId, _.object([key], [value])).then(function (res) {
        getFoods();
      }, function (res) {
        console.log(res);
      });
    };

    /**
     *  Watchers
     */
    $scope.$watch('currentPage', function () {
      getFoods();
    });
    $scope.$watchGroup(['orderby', 'asc'], function () {
      $scope.currentPage = 1;
      getFoods();
    });

    /**
     * Init
     */
    getFoods();
  }).directive("editableTable", function () {
    return {
      restrict: "E",
      scope: { items: "=", headers: "=", orderby: "=", asc: "=", update: "=" },
      template: "\n        <table class=\"ui sortable celled compact collapsing small table very basic\">\n          <thead>\n          <tr>\n              <th ng-repeat=\"header in headers\" ng-click=\"($parent.orderby === header) && ($parent.asc = !$parent.asc) || ($parent.orderby = header)\">{{header | i18n}}</th>\n          </tr>\n          </thead>\n          <tbody>\n          <tr ng-repeat=\"item in items\">\n              <td ng-repeat=\"(key, value) in item\" style=\"text-transform: capitalize;\" ng-if=\"key !== '_id'\" ng-init=\"selected = false\" ng-click=\"selected = true\">\n              <div ng-if=\"!selected\">\n                {{value}}\n              </div>\n              <div ng-if=\"selected\">\n                <form ng-submit=\"update(item._id, key, updateValue)\">\n                  <div class=\"ui input\" ng-init=\"updateValue = value\">\n                    <input type=\"text\" autofocus ng-model=\"updateValue\" style=\"padding:0;width:100%;\"/>\n                  </div>\n                </form>\n              </div>\n              </td>\n          </tr>\n          </tbody>\n      </table>\n        "
    };
  });
})();

//# sourceMappingURL=foods-compiled.js.map