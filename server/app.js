(function () {
  "use strict";
  let express = require('express');
  var bodyParser = require('body-parser');
  let mongoose = require('mongoose');
  let uuid = require('node-uuid');
  let Foods = require("./models/foods");
  let fs = require("fs");
  let csv = require("fast-csv");
  let _ = require("underscore");

  let port = 3001;
  let app = express();

  app.use(bodyParser.json());
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  mongoose.connect('mongodb://127.0.0.1:27017/food');
  let db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function () {
    console.log("connected to MongoDB.");
    initData();
  });

  /**
   * Routes
   */
  app.get('/foods', function (req, res) {
    let s = Number(req.query.skip), l = Number(req.query.limit), orderby = req.query.orderby, asc = req.query.asc;
    let search = req.query.search;
    let skip = (Number.isNaN(s) || s < 0) ? 0 : s, limit = (Number.isNaN(l) || l) < 0 ? 0 : l;
    if (1000 < limit) {
      console.log("The limit has reached the max number, 1000. It will now return max 1000 items instead.");
      limit = 1000;
    }
    let select = {}, sort = {}, projection = {};
    if (orderby) {
      sort[orderby] = (asc === "true") ? 1 : -1;
    }
    if (search && search.trim() !== "") {
      select["$text"] = {$search: search};
      projection["score"] = {$meta: "textScore"};
      sort["score"] = {$meta: "textScore"};
    } else {
      sort["updated_at"] = -1;
    }
    Foods.find(select, projection, {skip: skip, limit: limit, sort: sort}, function (err, docs) {
      handleResponse(res, err, docs);
    });
  });

  app.post('/foods/:id', function (req, res) {
    let id = req.params.id || null;
    let body = req.body || {};
    Foods.findByIdAndUpdate(id, body, function (err, docs) {
      handleResponse(res, err, docs);
    });
  });

  /**
   * Listener
   */
  app.listen(port, function () {
    console.log(`Server is listening on port ${port}!`);
  });


  /**
   * Helpers
   */
  function handleResponse(res, err, data) {
    if (err) {
      handleError(res, err);
    } else {
      handleSuccess(res, data);
    }
  }

  function handleError(res, err) {
    let status = 400;
    console.error(err);
    res.status(status);
    res.json([]);
  }

  function handleSuccess(res, data) {
    let status = 200;
    res.status(status);
    res.json(data);
  }

  function initData() {
    Foods.findOne({}, function (err, res) {
      if (res) {
        console.log("Init data already initialized");
      } else {
        let stream = fs.createReadStream("foods.csv");
        let objs = [];
        csv
          .fromStream(stream, {headers: true, ignoreEmpty: true, trim: true})
          .on("data", function (data) {
            data = _.mapObject(data, function (val) {
              if (!Number.isNaN(Number(val))) return Number(val);
              if (!val) return null;
              return val.trim().toLowerCase();
            });
            data.uuid = uuid.v4().toString();
            data.created_at = new Date();
            data.updated_at = new Date();
            objs.push(data);
          })
          .on("end", function () {
            Foods.collection.insertMany(objs, function (err) {
              if (err) {
                console.error("InitData Error", err);
              }
              console.log("Init Done");
            });
          });
      }
    });
  }
}());