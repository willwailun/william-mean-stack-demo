(function () {
  "use strict";
  let mongoose = require('mongoose');
  let uuid = require('node-uuid');
  let Schema = mongoose.Schema;

  /**
   * Schema
   */
  let foodSchema = new Schema({
    name: String,
    kcal: Number,
    protein_g: Number,
    fat_g: Number,
    carb_g: Number,
    fiber_g: Number,
    sugar_g: Number,
    calcium_mg: Number,
    iron_mg: Number,
    sodium_mg: Number,
    cholesterol_mg: Number,
    uuid: {
      type: String,
      default: uuid.v4().toString()
    },
    created_at: {
      type: Date,
      default: new Date()
    },
    updated_at: {
      type: Date,
      default: new Date()
    }
  });

  /**
   * Indexes
   */
  foodSchema.index({name: "text"});

  /**
   * Triggers
   */
  foodSchema.pre('save', function (next) {
    this.updated_at = new Date();
    next();
  });

  /**
   * Export
   */
  let Foods = mongoose.model('Foods', foodSchema);
  module.exports = Foods;
})();