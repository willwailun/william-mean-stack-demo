# Demo
Tested with latest chrome and safari. Not optimize for mobile-use.

[Know Your Food](http://bit.ly/1XbVJWx)

# Frameworks/Tools Used
MongoDB, Express.js, AngularJS, Node.js, Semantic-UI

AWS-EC2, Nginx

# Start Server

```bash
cd server
npm install
node app.js
```

# Start Web

```bash
cd web
npm start
```